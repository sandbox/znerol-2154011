<?php
/**
 * @file
 * Defines default views.
 */

/**
 * Implements hook_views_default_views_alter().
 */
function authcache_commerce_kickstart_views_default_views_alter(&$views) {
  if (module_exists('authcache_views')) {
    foreach (array('shopping_cart', 'shopping_cart_block', 'shopping_cart_form') as $view_name) {
      if (isset($views[$view_name])) {
        foreach (array_keys($views[$view_name]->display) as $display_name) {
          $views[$view_name]->display[$display_name]->display_options['authcache'] = array(
            'status' => TRUE,
            'lifespan' => 3600,
            'peruser' => 1,
            'perpage' => 0,
            'fallback' => 'cancel',
          ) + authcache_p13n_config_defaults();
        }
      }
    }
  }
}
